from django.apps import AppConfig


class ApptascasConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apptascas'
