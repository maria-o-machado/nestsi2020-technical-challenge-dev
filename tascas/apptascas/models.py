from django.db import models

# Create your models here.
class Tasca(models.Model):
    nome = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    rating = models.IntegerField(default=0)
    latitude = models.FloatField(default=0)
    longitude = models.FloatField(default=0)

