from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('deleteTasca', views.deleteTasca, name='deleteTasca'),
    path('createTasca', views.createTasca, name='createTasca'),
    path('addTasca', views.addTasca, name='addTasca'),
    path('editTasca', views.editTasca, name='editTasca'),
    path('updateTasca', views.updateTasca, name='updateTasca'),
    path('cancel', views.cancel, name='cancel'),
    path('details/<int:pk>', views.detailsTasca, name='detailsTasca'),
]