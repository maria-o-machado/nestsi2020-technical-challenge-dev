from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import *


# Create your views here.
def home(request):
    tascas = Tasca.objects.all()
    return render(request, 'initial.html',{'tascas': tascas})

#Apagar uma tasca
def deleteTasca(request):
    if request.method == 'POST':
        tasca_id = request.POST.get('deleteTasca')
        Tasca.objects.filter(id=tasca_id).delete()

    return redirect('/')

def editTasca(request):
    if request.method == 'POST':
        tasca_id = request.POST.get('editTasca')
        tasca = Tasca.objects.get(id=tasca_id)
    return render(request, 'editTasca.html',{'tasca': tasca})

def updateTasca(request):
    if request.method == 'POST':
        tasca_id = request.POST.get('editTasca')
        nome = request.POST['name']
        address = request.POST['address']
        rating = request.POST['rating']
        latitude = request.POST['latitude']
        longitude = request.POST['longitude']
        tasca = Tasca.objects.filter(id=tasca_id)
        tasca.update(nome=nome, address=address, rating=rating, latitude=latitude, longitude=longitude)
    
    return redirect('/')

#Criar uma nova tasca
def createTasca(request):
    return render(request, 'createTasca.html')

def addTasca(request):
    if request.method == 'POST':
        nome = request.POST['name']
        address = request.POST['address']
        rating = request.POST['rating']
        latitude = request.POST['latitude']
        longitude = request.POST['longitude']
        tasca = Tasca.objects.create(nome=nome, address=address, rating=rating, latitude=latitude, longitude=longitude)
        tasca.save()

    return redirect('/')

def detailsTasca(request, pk):
    if request.method == 'GET':
       tasca = Tasca.objects.get(id=pk)

    return render(request, 'detailsTasca.html',{'tasca': tasca})

#Botao de cancelar no editar e no create
def cancel(request):
    return redirect('/')