# intershipNEST2021

## Setup
Clone the repository:

$ git clone https://github.com/maria-o-machado/intershipNEST2021.git

$ cd intershipNEST2021/tascas


Create a virtual environment. Then install the dependencies:

(env)$ pip install django

Note the (env) in front of the prompt. This indicates that this terminal session operates in a virtual environment set up by virtualenv2.


## Run the project:

(env)$ python manage.py makemigrations

(env)$ python manage.py migrate

(env)$ python manage.py runserver
